package com.devcamp.relationship.model;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "tags")
public class CHashTag {
	public CHashTag(String name) {
		super();
		this.name = name;
	}

	public CHashTag() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name")
	private String name;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "tags")
	private Set<CPost> posts;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<CPost> getPosts() {
		return posts;
	}

	public void setPosts(Set<CPost> posts) {
		this.posts = posts;
	}

}
